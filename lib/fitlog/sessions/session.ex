defmodule Fitlog.Sessions.Session do
  use Ecto.Schema
  import Ecto.Changeset
  alias Fitlog.{Sessions.Session, Users.User}

  schema "user_sessions" do
    belongs_to :users, User, type: :integer, foreign_key: :user_id
    field :access_token, Ecto.UUID, null: false
    field :access_token_expires_at, Timex.Ecto.DateTime, null: false
    timestamps()
  end

  @doc false
  def changeset(%Session{} = sessions, attrs) do
    sessions
    |> cast(attrs, [:user_id, :access_token, :access_token_expires_at])
    |> validate_required([:user_id, :access_token, :access_token_expires_at])
    |> foreign_key_constraint(:user_id, message: "User not specified")
  end
end
