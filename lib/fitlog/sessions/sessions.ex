defmodule Fitlog.Sessions do
    import Fitlog.Monad.Result
    alias Comeonin.Pbkdf2
    alias Fitlog.{Repo, Users, Users.User, Sessions.Session}

    @spec authenticate(String.t, String.t) :: map
    def authenticate(email, password) do
        Users.get_user_by_email(email)
        |> valid_password?(password)
        |> on_ok(&create_session/1)
        |> on_error(fn _ -> {:error, "Invalid username/password."} end)
    end

    @spec authenticate(String.t, String.t) :: map
    def logout(access_token) do
        access_token
        |> Ecto.UUID.cast
        |> on_ok(&get_session/1)
        |> on_ok(&expire_session/1)
    end

    @spec get_authed_user(String.t) :: map
    def get_authed_user(access_token) do
        access_token
        |> Ecto.UUID.cast 
        |> on_ok(&get_session/1)
        |> on_ok(&check_active/1)
        |> on_ok(&Users.get_user(&1.user_id))
    end

    defp valid_password?({:ok, user}, password), do: Pbkdf2.check_pass(user, password)
    defp valid_password?(error, _) do
        Pbkdf2.dummy_checkpw() 
        error
    end

    defp create_session(%User{id: user_id}) do
        params = %{
            user_id: user_id, 
            access_token: Ecto.UUID.generate(),
            access_token_expires_at: Timex.shift(Timex.now(), days: 1)
        }
        
        Session.changeset(%Session{}, params)
        |> Repo.insert
    end

    defp get_session(access_token) do
        #throws error if more than one exists - but this shouln't happen here
        result = Repo.get_by(Session, access_token: access_token)
        case result do
            nil  -> error "Session does not exist"
            session -> ok session
        end
    end

    defp check_active(session) do
        if Timex.compare(session.access_token_expires_at, Timex.now()) == -1 
            do error("Access token has expired")
            else ok(session)
        end
    end

    defp expire_session(session = %Session{}) do
        params = %{ access_token_expires_at: Timex.now() }
        
        session
        |> Session.changeset(params)
        |> Repo.update
    end
end