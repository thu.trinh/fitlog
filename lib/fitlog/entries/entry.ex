defmodule Fitlog.Entries.Entry do
  use Ecto.Schema
  import Ecto.Changeset
  alias Fitlog.{Entries.Entry, Users.User}


  schema "user_entries" do
    belongs_to :users, User, type: :integer, foreign_key: :user_id
    field :tags, :string
    field :weight, :float

    timestamps()
  end

  @doc false
  def changeset(%Entry{} = entry, attrs) do
    entry
    |> cast(attrs, [:user_id, :weight, :tags])
    |> validate_required([:user_id, :weight, :tags])
    |> foreign_key_constraint(:user_id, message: "User not specified")
  end
end
