defmodule Fitlog.Users do
    import Fitlog.Monad.Result
    alias Comeonin.Pbkdf2
    alias Fitlog.{Repo, Users.User}

    def create_user(attrs) do
        validate_password(attrs)
        |> on_ok(&put_pass_hash/1)
        |> on_ok(&insert/1)
        |> user?
    end

    def get_user(id), do: Repo.get(User, id) |> user?
    def get_user_by_email(email), do: Repo.get_by(User, email: email) |> user?

    defp user?({:ok, user}), do: user |> ok
    defp user?({:error, changeset}), do: changeset.errors |> error
    defp user?(%User{} = user), do: ok user
    defp user?(_), do: error "User does not exist"

    defp validate_password(%{password: password} = attrs) do
        valid_password?(password)
        |> on_ok(fn _ -> ok(attrs) end)
    end
    defp validate_password(_), do: {:error, "The password is required"}

    defp valid_password?(password) when byte_size(password) > 7, do: {:ok, password}
    defp valid_password?(_), do: {:error, "The password is too short"}

    defp put_pass_hash(%{password: password} = attrs) do 
        Map.merge(attrs, Pbkdf2.add_hash(password)) 
        |> ok
    end

    defp insert(attrs) do
        User.changeset(%User{}, attrs) 
        |> Repo.insert
    end
end