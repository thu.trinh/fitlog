defmodule Fitlog.Users.User do
  use Ecto.Schema
  import Ecto.Changeset
  alias Fitlog.{Users.User, Sessions.Session, Entries.Entry}

  schema "users" do
    field :email, :string
    field :full_name, :string
    field :password_hash, :string
    timestamps()
    has_many :user_sessions, Session
    has_many :user_entries, Entry
  end

  @doc false
  def changeset(%User{} = user, attrs) do
    user
    |> cast(attrs, [:full_name, :email, :password_hash])
    |> validate_required([:full_name, :email, :password_hash])
    |> validate_length(:full_name, min: 2)
    |> validate_format(:email, ~r/(\w+)@([\w.]+)/)
    |> unique_constraint(:email, message: "Email already exists")
  end
end
