defmodule Fitlog.Monad.Result do
    def ok(), do: {:ok}
    def ok(t), do: {:ok, t}
    def error(t), do: {:error, t}
    def ok!({:ok, t}), do: t
    def error!({:error, t}), do: t

    def on_ok(t, ok_fn) do
        case t do
            {:ok, x} -> ok_fn.(x)
            x -> x
        end
    end

    def on_error(t, error_fn) do
        case t do
            {:error, x} -> error_fn.(x)
            x -> x
        end
    end
end