defmodule FitlogWeb.SessionView do
    use FitlogWeb, :view

    def render("create.json", %{session: session}) do
        %{
            access_token: session.access_token,
            expires: Timex.format!(session.access_token_expires_at, "{ISO:Extended}")
        }
    end
end