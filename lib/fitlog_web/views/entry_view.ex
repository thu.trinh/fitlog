defmodule FitlogWeb.EntryView do
  use FitlogWeb, :view
  alias FitlogWeb.EntryView

  def render("index.json", %{entries: entries}) do
    %{data: render_many(entries, EntryView, "entry.json")}
  end

  def render("show.json", %{entry: entry}) do
    %{data: render_one(entry, EntryView, "entry.json")}
  end

  def render("entry.json", %{entry: entry}) do
    %{id: entry.id,
      weight: entry.weight,
      tags: entry.tags}
  end
end
