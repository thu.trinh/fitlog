defmodule FitlogWeb.Router do
  use FitlogWeb, :router
  alias FitlogWeb.Plug

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  pipeline :auth do
    plug Plug.AuthorizationPlug
  end

  scope "/", FitlogWeb do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :index
  end

  # Other scopes may use custom stacks.
  scope "/api", FitlogWeb do
    pipe_through :api
    post "/auth/sign-in", SessionController, :create
  end

  scope "/api", FitlogWeb do
    pipe_through [:api, :auth]
    delete "/auth/sign-out", SessionController, :delete
    resources "/entries", EntryController
  end
end
