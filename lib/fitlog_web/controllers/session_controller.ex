defmodule FitlogWeb.SessionController do
  alias Fitlog.Sessions
  alias FitlogWeb.ErrorView
  use FitlogWeb, :controller

  def create(conn, %{"password" => p, "username" => u}) do
    case Sessions.authenticate(u, p) do
      {:ok, s} -> render(conn, "create.json", session: s)

      {:error, r} -> 
        conn
        |> put_status(400)
        |> render(ErrorView, "error.json", message: r)
    end
  end

  def delete(conn, _params) do
    result = 
        conn
        |> get_req_header("authorization")
        |> List.first
        |> Sessions.logout

    case result do
      {:ok, _} -> send_resp(conn, :ok, "")
      {:error, msg} -> render(ErrorView, "error.json", message: msg)
    end
  end
end