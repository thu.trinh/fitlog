defmodule FitlogWeb.PageController do
  use FitlogWeb, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
