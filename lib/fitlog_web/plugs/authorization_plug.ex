defmodule FitlogWeb.Plug.AuthorizationPlug do
    import Plug.Conn
    alias Fitlog.{Sessions}
  
    def init(opts), do: opts
  
    def call(conn, _opts) do
        user_result = 
            conn
            |> get_req_header("authorization")
            |> List.first
            |> Sessions.get_authed_user
            
        case user_result do
            {:ok, u} -> 
                conn 
                |> assign(:user_id, u.id)

            _ -> 
                conn
                |> put_status(:unauthorized)
                |> halt()
        end
    end
  end
  