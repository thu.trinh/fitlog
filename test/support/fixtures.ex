defmodule Fitlog.Fixtures do
    alias Fitlog.Repo
    alias Comeonin.Pbkdf2
    alias Fitlog.{Users.User, Sessions.Session, Entries.Entry}

    @user_attr %{
        full_name: "John Doe",
        email: "john.doe@example.com",
        password_hash: "xxx-xxx-xxx"
    }

    def user_attr, do: @user_attr

    @create_user_attr %{
        full_name: "John Doe",
        email: "john.doe@example.com",
        password: "12345678"
    }
    def create_user_attr, do: @create_user_attr

    def fixture(entity, assoc \\ [])

    def fixture(:user, [{:user_id, user_id}]), do: Repo.get!(User, user_id)
    def fixture(:user, assoc) do
        default = Map.merge(@create_user_attr, Pbkdf2.add_hash(@create_user_attr.password))
        
        User.changeset(%User{}, assoc[:user] || default)
        |> Repo.insert!
    end
    def fixture(:user_password, _assoc), do: @create_user_attr.password

    def fixture(:session, [{:access_token, access_token}]), do: Repo.get_by!(Session, access_token: access_token)
    def fixture(:session, assoc) do
        user_id = assoc[:user_id] || fixture(:user).id

        %Session{
            user_id: user_id, 
            access_token: Ecto.UUID.generate(),
            access_token_expires_at: Timex.shift(Timex.now(), days: 1)
        }
        |> Repo.insert! 
    end

    def fixture(:entry, [{:id, id}]), do: Repo.get!(Entry, id)
    def fixture(:entry, assoc) do
        user_id = assoc[:user_id] || fixture(:user).id

        %Entry{
            user_id: user_id, 
            tags: assoc[:tags] ||"some tags", 
            weight: assoc[:weight] || 120.5
        }
        |> Repo.insert!
    end
end