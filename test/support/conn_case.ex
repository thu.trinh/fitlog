defmodule FitlogWeb.ConnCase do
  @moduledoc """
  This module defines the test case to be used by
  tests that require setting up a connection.

  Such tests rely on `Phoenix.ConnTest` and also
  import other functionality to make it easier
  to build common datastructures and query the data layer.

  Finally, if the test case interacts with the database,
  it cannot be async. For this reason, every test runs
  inside a transaction which is reset at the beginning
  of the test unless the test case is marked as async.
  """

  use ExUnit.CaseTemplate
  import Plug.Conn

  using do
    quote do
      # Import conveniences for testing with connections
      use Phoenix.ConnTest
      import FitlogWeb.Router.Helpers

      # The default endpoint for testing
      @endpoint FitlogWeb.Endpoint
    end
  end

  setup tags do
    :ok = Ecto.Adapters.SQL.Sandbox.checkout(Fitlog.Repo)
    unless tags[:async] do
      Ecto.Adapters.SQL.Sandbox.mode(Fitlog.Repo, {:shared, self()})
    end

    {:ok, conn: Phoenix.ConnTest.build_conn() }
  end

  #Add api headers and ommit the :auth pipeline
  def api(conn) do
    conn
    |> put_req_header("content-type", "application/json")
    |> put_req_header("accepts", "application/json")
  end
  
  def authorize(conn, access_token) do
    put_req_header(conn, "authorization", access_token)
  end

  def access_token(conn) do
    conn
    |> get_req_header("authorization")
    |> List.first
  end
end
