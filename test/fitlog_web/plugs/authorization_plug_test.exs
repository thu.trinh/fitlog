defmodule FitlogWeb.Plugs.AuthorizationPlugTest do
    use FitlogWeb.ConnCase
    alias FitlogWeb.{Plug.AuthorizationPlug,Router}
    alias Fitlog.{Repo, Users.User, Sessions.Session}
  
    describe "authorization plug" do
      test "acess_token does not exists for route that requires auth", %{conn: conn} do
        conn = 
            conn 
            |> bypass_through(Router, [:api]) 
            |> get("/api/entries") 
            |> AuthorizationPlug.call([]) 

        assert conn.halted 
      end

      test "acess_token exists for route that requires auth", %{conn: conn} do
        access_token = Ecto.UUID.generate()
        user_attr = %{full_name: "John", email: "john@example.com", password_hash: "12345678"}
        user = 
            User.changeset(%User{}, user_attr)
            |> Repo.insert!

        session_attr = %{user_id: user.id, access_token: access_token, access_token_expires_at: Timex.shift(Timex.now(), days: 1)}
        Session.changeset(%Session{}, session_attr)
        |> Repo.insert!

        %{assigns: assigns} =
            conn 
            |> bypass_through(Router, [:api]) 
            |> put_req_header("authorization", access_token)
            |> get("/api/entries") 
            |> AuthorizationPlug.call([])

        assert %{user_id: user.id} == assigns
      end
    end
  end
  