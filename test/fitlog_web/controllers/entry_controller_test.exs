defmodule FitlogWeb.EntryControllerTest do
  use FitlogWeb.ConnCase

  import Fitlog.Fixtures
  alias Fitlog.Entries.Entry
  alias FitlogWeb.ConnCase
  
  @create_attrs %{tags: "some tags", weight: 120.5}
  @update_attrs %{tags: "some updated tags", weight: 456.7}
  @invalid_attrs %{tags: nil, weight: nil}

  setup %{conn: conn} do
    session = fixture(:session)

    { :ok, 
      conn: conn 
        |> ConnCase.api() 
        |> ConnCase.authorize(session.access_token)
      }
  end

  describe "index" do
    test "lists all entries", %{conn: conn} do
      response = 
        conn
        |> get(entry_path(conn, :index))
        |> json_response(200)

      assert response["data"] == []
    end
  end

  describe "create entry" do
    test "renders entry when data is valid", %{conn: conn} do
      created = 
        conn
        |> post(entry_path(conn, :create), entry: @create_attrs)
        |> json_response(201)

      assert %{"id" => id} = created["data"]

      entry = 
        conn
        |> get(entry_path(conn, :show, id))
        |> json_response(200)

      assert %{"id" => id, "tags" => "some tags", "weight" => 120.5} == entry["data"]
    end

    test "renders errors when data is invalid", %{conn: conn} do
        response = post(conn, entry_path(conn, :create), entry: @invalid_attrs)
      
        assert json_response(response, 422)["errors"] != %{}
    end
  end

  describe "update entry" do
    setup [:create_entry]

    test "renders entry when data is valid", %{conn: conn, entry: %Entry{id: id} = entry} do
      update_rsp = put conn, entry_path(conn, :update, entry), entry: @update_attrs
      assert %{"id" => ^id} = json_response(update_rsp, 200)["data"]

      get_resp = get conn, entry_path(conn, :show, id)
      assert json_response(get_resp, 200)["data"] == %{
        "id" => id,
        "tags" => "some updated tags",
        "weight" => 456.7}
    end

    test "renders errors when data is invalid", %{conn: conn, entry: entry} do
      conn = put conn, entry_path(conn, :update, entry), entry: @invalid_attrs
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete entry" do
    setup [:create_entry]

    test "deletes chosen entry", %{conn: conn, entry: entry} do
      delete_rsp = delete conn, entry_path(conn, :delete, entry)
      assert response(delete_rsp, 204)
      assert_error_sent 404, fn ->
        get conn, entry_path(conn, :show, entry)
      end
    end
  end

  defp create_entry(%{conn: conn}) do
    access_token = ConnCase.access_token(conn)
    session = fixture :session, access_token: access_token
    entry = fixture(:entry, user_id: session.user_id)

    {:ok, entry: entry}
  end
end
