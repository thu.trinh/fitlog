defmodule FitlogWeb.SessionControllerTest do
    import Fitlog.Fixtures
    use FitlogWeb.ConnCase
    alias Fitlog.{Repo, Sessions.Session}
    alias FitlogWeb.ConnCase
    
    setup %{conn: conn} do
        {:ok, conn: ConnCase.api(conn)}
    end

    describe "create/2" do
        test "Creates, and responds with a newly created session if attributes are valid", %{conn: conn} do
            user = fixture(:user)
            password = fixture(:user_password)
            
            payload = 
                %{
                    "username" => user.email,
                    "password" => password
                }
                |> Poison.encode!

            %{"access_token" => access_token} = 
                conn
                |> post(session_path(conn, :create), payload)
                |> json_response(200)

            assert access_token != ""
            assert access_token != nil
        end

        test "Returns an error and does not create a session if attributes are invalid", %{conn: conn} do
            payload = 
                %{
                    "username" => "Not exists", 
                    "password" => "random"
                } 
                |> Poison.encode!

            %{"error" => error} = 
                conn
                |> post(session_path(conn, :create), payload)
                |> json_response(400)

            assert error == %{"detail" => "Invalid username/password."}
        end
    end

    test "delete/2 and responds with :ok if the user was deleted", %{conn: conn} do
        session = fixture(:session)
        auth_conn = ConnCase.authorize(conn, session.access_token)
        
        auth_conn
        |> delete(session_path(auth_conn, :delete))
        |> response(200)

        altered_session = Repo.get!(Session, session.id)
        assert Timex.compare(session.access_token_expires_at, Timex.now()) == 1
        assert Timex.compare(altered_session.access_token_expires_at, Timex.now()) == -1
    end
  end