defmodule Fitlog.EntriesTest do
  import Fitlog.Fixtures
  use Fitlog.DataCase

  alias Fitlog.Entries

  describe "entries" do
    alias Fitlog.Entries.Entry

    @valid_attrs %{tags: "some tags", weight: 120.5}
    @update_attrs %{tags: "some updated tags", weight: 456.7}
    @invalid_attrs %{tags: nil, weight: nil}

    test "list_entries/0 returns all entries" do
      entry = fixture(:entry)
      assert Entries.list_entries() == [entry]
    end

    test "get_entry!/1 returns the entry with given id" do
      entry = fixture(:entry)
      assert Entries.get_entry!(entry.id) == entry
    end

    test "create_entry/1 with valid data creates a entry" do
      assert {:ok, %Entry{} = entry} = Entries.create_entry(user_entry_attr(@valid_attrs))
      assert entry.tags == @valid_attrs[:tags]
      assert entry.weight == @valid_attrs[:weight]
    end

    test "create_entry/1 with invalid data returns error changeset" do
      result = 
        @invalid_attrs
        |> user_entry_attr()
        |> Entries.create_entry()

      assert {:error, %Ecto.Changeset{}} = result
    end

    test "update_entry/2 with valid data updates the entry" do
      orig_entry = fixture(:entry)
      assert {:ok, entry} = Entries.update_entry(orig_entry, @update_attrs)
      assert %Entry{} = entry
      assert @update_attrs[:tags] == entry.tags
      assert @update_attrs[:weight] == entry.weight
    end

    test "update_entry/2 with invalid data returns error changeset" do
      entry = fixture(:entry)
      assert {:error, %Ecto.Changeset{}} = Entries.update_entry(entry, @invalid_attrs)
      assert entry == Entries.get_entry!(entry.id)
    end

    test "delete_entry/1 deletes the entry" do
      entry = fixture(:entry)
      assert {:ok, %Entry{}} = Entries.delete_entry(entry)
      assert_raise Ecto.NoResultsError, fn -> Entries.get_entry!(entry.id) end
    end

    test "change_entry/1 returns a entry changeset" do
      entry = fixture(:entry)
      assert %Ecto.Changeset{} = Entries.change_entry(entry)
    end
  end

  defp user_entry_attr(for_attr) do
    user = fixture(:user)
    Map.merge(for_attr, %{user_id: user.id})
  end
end
