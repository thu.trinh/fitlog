defmodule FitlogWeb.UserTest do
    use Fitlog.DataCase
    import Fitlog.Monad.Result
    alias Fitlog.{Users, Users.User, Fixtures}
    alias Comeonin.Pbkdf2
  
    test "changeset with valid attributes" do
      changeset = User.changeset(%User{}, Fixtures.user_attr)
      assert changeset.valid?
    end
  
    test "changeset with invalid attributes" do
      changeset = User.changeset(%User{}, %{})
      refute changeset.valid?
    end

    test "full_name must be more than 2 characters" do
        changeset = User.changeset(%User{}, %{Fixtures.user_attr | full_name: "a"})
        refute changeset.valid?
    end

    test "valid email" do
        refute User.changeset(%User{}, %{Fixtures.user_attr | email: "aa aa aa"}).valid?
        assert User.changeset(%User{}, %{Fixtures.user_attr | email: "some.name@exmple.com"}).valid?
    end

    test "create user with valid attributes" do
        result = Users.create_user(Fixtures.create_user_attr)
        assert {:ok, _} = result
        
        password = Fixtures.create_user_attr[:password]
        password_hash = ok!(result).password_hash
        assert Pbkdf2.checkpw(password, password_hash)
      end
  end