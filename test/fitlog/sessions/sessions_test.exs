defmodule FitlogWeb.SessionTest do
    use Fitlog.DataCase
    alias Fitlog.Sessions.Session
  
    @moduletag :schema

    @valid_attrs %{
        access_token: Ecto.UUID.generate(),
        access_token_expires_at: Timex.shift(Timex.now(), days: 1),
        user_id: 1
    }
  
    test "changeset with valid attributes" do
      changeset = Session.changeset(%Session{}, @valid_attrs)
      assert changeset.valid?
    end

    test "changeset with invalid attributes" do
        changeset = Session.changeset(%Session{}, %{})
        refute changeset.valid?
      end
  end