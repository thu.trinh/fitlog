defmodule Fitlog.Repo.Migrations.InitData do
  alias Comeonin.Pbkdf2
  alias Fitlog.{Repo, Users.User}
  use Ecto.Migration

  def change do
    params = %{
      full_name: "Jane Do",
      email: "jane@example.com",
      password_hash: Pbkdf2.hashpwsalt("12345678")
    }
    
    User.changeset(%User{}, params)
    |> Repo.insert
  end
end