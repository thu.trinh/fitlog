defmodule Fitlog.Repo.Migrations.CreateUsers do
  use Ecto.Migration

  def change do
    create table(:users) do
      add :full_name, :string
      add :email, :string
      add :password_hash, :string

      timestamps()
    end
    create unique_index(:users, [:email])

    create table(:user_sessions) do
      add :user_id, references(:users, type: :integer, null: false)  
      add :access_token, :uuid
      add :access_token_expires_at, :utc_datetime
      timestamps()
    end

    create table(:user_entries) do
      add :user_id, references(:users, type: :integer, null: false)  
      add :weight, :float
      add :tags, :string

      timestamps()
    end
  end
end
